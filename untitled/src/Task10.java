import java.util.Random;
import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        playGame ();
    }

    public static void playGame(){
        Scanner scanner = new Scanner (System.in);
        Random ran = new Random();
        int randomNumber = ran.nextInt(1000);
        System.out.println ("Компьютер загадал число, отгадайте его");
        int numberOfPeople = scanner.nextInt ();

        if (numberOfPeople != randomNumber) {
            while (numberOfPeople != randomNumber) {
                if (numberOfPeople < 0){
                    System.out.println ("Вы закончили игру.");
                    break;
                }
                else if (numberOfPeople < randomNumber) {
                    System.out.println ("Это число меньше загаданного.");
                    numberOfPeople = scanner.nextInt ();
                } else if (numberOfPeople > randomNumber) {
                    System.out.println ("Это число больше загаданного.");
                    numberOfPeople = scanner.nextInt ();
                }
            }
        }
        System.out.println ("Победа!");
    }
}
